package tech.quantit.northstar.common.constant;

public enum FieldType {

	TEXT,
	
	PASSWORD,
	
	NUMBER,
	
	DATE,
	
	SELECT,
	
	MULTI_SELECT;
}
